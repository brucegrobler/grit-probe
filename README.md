## Build on Raspberry PI

```
apt install cmake build-essential libyaml-cpp-dev libnet1-dev libmosquittopp-dev mosquitto-dev
cd grit-probe
mkdir out
cmake ..
make
```


## Install  

```
mkdir -p /etc/grit/probe
mkdir -p /opt/grit/probe/bin
mkdir -p /opt/grit/probe/etc

cp grit-probe/etc/probe.example.yaml /opt/grit/probe/probe.yaml
ln -s /opt/grit/probe/probe.yaml /etc/grit/probe/probe.yaml

cp out/grit-probe /opt/grit/probe/bin/

# install service systemd
sudo cp grit-probe.service /etc/systemd/system/grit-probe.service
sudo chmod 644 /etc/systemd/system/grit-probe.service

# enable and start service
sudo systemctl enabled grit-probe.service
sudo systemctl start grit-probe.service



if voltronic! 

[18:09:35] openhabian@openHABianPi:/opt/grit/probe/bin$ cat /etc/udev/rules.d/50-voltronic.conf
SUBSYSTEM=="usb", ATTR{idVendor}=="0665", ATTR{idProduct}=="5161", MODE="0666"
[18:09:42] openhabian@openHABianPi:/opt/grit/probe/bin$

if victron bmv 

# check that ttyUSB0 is the bmv - lsusb / dmesg.
echo "stty -F /dev/ttyUSB0 19200" >> /etc/rc.local


```


### Running 
```z

/opt/grit/probe/bin/grit-probe -d 

```

## Arguments 

`-d`        *run as a daemon*


## Includes

https://github.com/fanzhe98/modbuspp
