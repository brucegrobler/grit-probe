#!/bin/bash
WLOG=watchdog.log
cd /opt/grit/probe/bin/
while true ; do
    echo "`date` grit-probe - watchdog - check " >> $WLOG
    pids=`ps aux | grep -v 'grep' | grep 'grit-probe -d' | head -10 | awk '{ print $2 }'`
    if [[ -z ${pids} ]]; then
        echo "`date` grit-probe - watchdog - startup " >> $WLOG
        ./grit-probe -d
        echo "`date` grit-probe - watchdog - started " >> $WLOG
        newPids=`ps aux | grep -v 'grep' | grep 'grit-probe -d' | head -10 | awk '{ print $2 }'`
        for pid in ${newPids} ; do
          echo "`date` grit-probe - watchdog - running as ${pid} " >> $WLOG
        done
    fi
sleep 5
done