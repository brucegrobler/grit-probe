#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include "main.h"
#include "util/utils.h"

#include <pthread.h>
#include <signal.h>
#include <cstring>

#include <ctime>
#include <libnet.h>
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <fstream>

#include "util/config.h"
#include "driver/victron-bmv.h"
#include "driver/tsmppt60.h"

#include "driver/axpert.h"
#include "mq/mqtt_client.h"

#include <syslog.h>

static const char *APP_VERSION = "1.1.0-SNAPSHOT";
static const char *CONFIG_FILE_DEFAULT = "/etc/grit/probe/probe.yaml";
static const char *APP_NAME = "grit-probe";

void heartbeat(MqttClient *mqtt_client);

static void skeleton_daemon() {

    pid_t pid;
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    if (setsid() < 0) {

        exit(EXIT_FAILURE);
    }
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);
    pid = fork();

    if (pid < 0) {

        exit(EXIT_FAILURE);
    }

    if (pid > 0) {

        exit(EXIT_SUCCESS);
    }


    umask(0);

    chdir("/tmp/");

    int x;
    for (x = sysconf(_SC_OPEN_MAX); x >= 0; x--) {
        close(x);
    }

    openlog(APP_NAME, LOG_PID, LOG_DAEMON);

    syslog(LOG_NOTICE, "Daemon running");
}

int main(int argc, char *argv[]) {

    openlog(APP_NAME, LOG_PID, LOG_DAEMON);
    bool run_as_daemon = false;

    syslog(LOG_NOTICE, "%s-%s starting up - config=[%s]", APP_NAME, APP_VERSION, CONFIG_FILE_DEFAULT);
    if (argc > 0) {

        for (int j = 0; j < argc; ++j) {
            syslog(LOG_DEBUG, "arg[%i]: [%s]", j, argv[j]);

            if (std::string(argv[j]) == "-d") {
                run_as_daemon = true;
            }

        }
    }
    if (run_as_daemon) {
        skeleton_daemon();
    }

    MqttClient *mqtt_client;

    Voltronic *voltronic;

    VictronBmv *victron_bmv;

    TsMppt60 *ts_mppt_60;

    Config *config;

    try {


        config = new Config(CONFIG_FILE_DEFAULT);

        // Manages it own threads..
        mqtt_client = new MqttClient(config);

        voltronic = new Voltronic(config, mqtt_client);

        victron_bmv = new VictronBmv(config, mqtt_client);

        ts_mppt_60 = new TsMppt60(config, mqtt_client);


        syslog(LOG_NOTICE, "%s-%s started", APP_NAME, APP_VERSION);

        while (true) {

            voltronic->start();
            victron_bmv->start();

            ts_mppt_60->start();
            heartbeat(mqtt_client);
        }

    } catch (const std::runtime_error &re) {
        std::cerr << "Runtime error: " << re.what() << std::endl;
        syslog(LOG_ERR, "Runtime Error: %s", re.what());
    }
    catch (const std::exception &ex) {
        std::cerr << "Error occurred: " << ex.what() << std::endl;
        syslog(LOG_ERR, "Error: %s", ex.what());
    }
    catch (...) {
        // catch any other errors (that we have no information about)
        std::cerr << "Unknown failure occurred. Possible memory corruption" << std::endl;
        syslog(LOG_ERR, "Unknown failure occurred. Possible memory corruption");
    }
}

void heartbeat(MqttClient *mqtt_client) {
    string now = Utils::current_date_time();
    mqtt_client->publish(NULL, "grit/probe/telemetry/currentDateTime", strlen(now.c_str()), now.c_str());
    sleep(10);
    syslog(LOG_DEBUG, "Heartbeat %s", now.c_str());
}



