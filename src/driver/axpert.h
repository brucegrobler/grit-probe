#ifndef ___AXPERT_H
#define ___AXPERT_H


static const char *const delimeter = " ";

#include <thread>
#include <mutex>
#include "../util/config.h"
#include "../mq/mqtt_client.h"

using namespace std;

class Voltronic {


    Config *config;
    MqttClient *mqtt_client;
    unsigned char buffer[1024]; //internal work buffer
    char status[1024];
    char mode;
    std::mutex m;

    void SetMode(char newmode);

    bool CheckCRC(unsigned char *buff, int len);

    bool query(const char *cmd, int reply_size);

    uint16_t cal_crc_half(uint8_t *pin, uint8_t len);

public:
    Voltronic(Config *config, MqttClient *mqtt_client);

    void poll();

    void start() {
        if (this->config->getAxpertEnabled()) {
            if (!isRunning) {
                isRunning = true;
                this->task = std::thread(&Voltronic::poll, this);

            }
        }
    }

    string *GetStatus();

    string *GetMode();

    void publishQpigs();

    bool isRunning = false;
    thread task;

    bool is_valid_crc(unsigned char *data, int len);

    void publishQmod();
};


class Qpigs {


public:
    Qpigs(const char *data);

private:
    string grid_voltage;
    string grid_frequency;
    string load_voltage;
    string load_apparent_power;
    string load_power;
    string load_percent;
    string bus_dc_voltage;
    string battery_voltage;
    string battery_charge_current;
    string battery_capacity;
    string heatsink_temperature;
    string mppt_current_1;
    string mppt_voltage_1;
    string battery_voltage_scc_1;
    string battery_discharge_current;
    string device_status_1;
    string sbu_priority_version;
    string configuration_updated;
    string scc_firmware_updated;
    string load_status;
    string battery_voltage_steady_under_charge;
    string charging_status_scc_1;
    string ac_charge_enabled;
    string charging_status;
    string battery_voltage_offset;
    string eeprom_version;
    string mppt_power_1;
    string device_status_2;
    string charging_float_mode;
    string power_switch_state;
    string reserved_1;
    string load_frequency;
    string device_charging_status;
    string battery_current;
    string battery_power;
    string grid_power;
public:


    string get_scc_firmware_updated();

    string get_load_status();

    string get_battery_voltage_steady_under_charge();

    string get_charging_status_scc_1();

    string get_ac_charge_enabled();

    string get_charging_status();

    string get_battery_voltage_offset();

    string get_eeprom_version();

    string get_charging_float_mode();

    string get_power_switch_state();

    string get_load_apparent_power();

    string get_bus_dc_voltage();

    string get_battery_charge_current();

    string get_battery_voltage_scc_1();

    string get_battery_discharge_current();

    string get_sbu_priority_version();

    string get_configuration_updated();

    string get_reserved_1();

    string get_load_voltage();

    string get_load_frequency();

    string get_load_percent();

    string get_load_power();

    string get_battery_voltage();

    string get_battery_capacity();

    string get_heatsink_temperature();

    string get_grid_voltage();

    string get_grid_frequency();

    string get_mppt_voltage_1();

    string get_mppt_power_1();

    string get_mppt_current_1();

    string get_battery_current();

    string get_battery_power();

    string get_grid_power();

    string get_est_load_power();
};


#endif // ___AXPERT_H
