//
// Created by bgrobler on 7/12/19.
//

#include "victron-bmv.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <list>
#include <sstream>
#include <syslog.h>
#include <iostream>
#include "../main.h"

#include "../util/utils.h"

#include "../util/config.h"

VictronBmv::VictronBmv(Config *config, MqttClient *mqtt_client) {
    if (config->getVictronBmvEnabled()) {
        this->config = config;
        this->mqtt_client = mqtt_client;
        syslog(LOG_DEBUG, "VictronBMV: Running %s", this->config->getVictronBmvPortFilename());
    }

}

void VictronBmv::poll() {

    while (isRunning) {
        syslog(LOG_DEBUG, "VictronBMV: Polling...\n");

        serialRead();

        sleep(config->getVictronBmvPollInterval());
    }
}

void VictronBmv::publish(const char *label, const char *value) {

    std::string topic = "victron/bmv/" + std::string(label);

    this->mqtt_client->publish(topic.c_str(), value);
    return;
}

void VictronBmv::serialRead() {
// TODO BG - There is a bug here where by the buffer  is not cleared and sends wierd data on the next poll.
// TODO BG - need to implement a global lock - grit probe does not like running multiple times - garbage on the file descriptors.

    int file_descriptor = open(config->getVictronBmvPortFilename(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (file_descriptor == -1) {
        syslog(LOG_ERR, "VictronBMV: Unable to open device file [%s] (errno=%d %s)\n",
               config->getVictronBmvPortFilename(), errno, strerror(errno));
        sleep(5);
        return;
    }

    int block_count = 0;
    int line_count = 0;

    const char delimiter[2] = "\t";

    if (file_descriptor) {

        while (block_count < READ_CHUNKS) {

            char line_buffer[LINE_LENGTH];

            int char_index = 0;


            while (char_index < LINE_LENGTH) {

                char buffer = 0;
                if (read(file_descriptor, &buffer, 1) > 0) {
                    // Wait for start sentinel.
                    if (buffer == '\n') {
                        // clear overflow
                        line_buffer[char_index] = '\0';
                        break;
                    }

                    // carriage return - ignore
                    if (buffer != 0x0d) {
                        line_buffer[char_index] = buffer;
                        char_index++;
                    }
                } else {
                    line_buffer[char_index] = '\0';
                }
            }

            line_count++;

            if (strncmp(line_buffer, "Checksum", 8) == 0) {
                line_buffer[8] = '\0';

                if (block_count <= 0) {
                    // Ignore the first block of data.
                    continue;
                }

                block_count++;
            }


            char *label = strtok(line_buffer, delimiter);
            char *value = strtok(nullptr, delimiter);
            if (label && value) {
                const char *parsed_value = parse_value(label, value);
                const char *mapped_label = get_mapped_label(label);

                syslog(LOG_DEBUG, "LINE: %i %i [%s] [%s]        [%s] [%s]",
                       block_count, line_count, label, mapped_label, value, parsed_value);

                // Extra data not sourced from bmv
                if (VEDIRECT_TTG == label) {
                    const char *ttg_hours = "TimeRemainingHours";
                    const char *ttg_hours_value = std::to_string((atof(value) / 60)).c_str();
                    publish(ttg_hours, ttg_hours_value);
                }

                publish(mapped_label, parsed_value);
            }
        }

    }
}

const char *VictronBmv::get_mapped_label(char *label) {

    if (VEDIRECT_V == label)
        return "BatteryVoltage";
    if (VEDIRECT_T == label)
        return "BatteryTemperature";
    if (VEDIRECT_I == label)
        return "BatteryCurrent";
    if (VEDIRECT_P == label)
        return "BatteryPower";
    if (VEDIRECT_CE == label)
        return "ConsumedAmpHours";
    if (VEDIRECT_SOC == label)
        return "StateOfCharge";
    if (VEDIRECT_TTG == label)
        return "TimeRemainingMinutes";
    if (VEDIRECT_ALARM == label)
        return "AlarmCondition";
    if (VEDIRECT_BMV == label)
        return "ModelDescription";
    if (VEDIRECT_FW == label)
        return "FirmwareVersion";
    if (VEDIRECT_H1 == label)
        return "DeepestDepthOfDischargeAmpHours";
    if (VEDIRECT_H2 == label)
        return "DepthOfLastDischargeAmpHours";
    if (VEDIRECT_H3 == label)
        return "DepthOfAverageDischargeAmpHours";
    if (VEDIRECT_H4 == label)
        return "ChargeCyclesCount";
    if (VEDIRECT_H5 == label)
        return "FullDischargeCount";
    if (VEDIRECT_H6 == label)
        return "CumulativeAmpHoursDrawn";
    if (VEDIRECT_H7 == label)
        return "MinBatteryVoltage";
    if (VEDIRECT_H8 == label)
        return "MaxBatteryVoltage";
    if (VEDIRECT_H9 == label)
        return "CurrentDischargeCycleDurationSeconds";
    if (VEDIRECT_H10 == label)
        return "AutomaticSynchronizationCount";
    if (VEDIRECT_H11 == label)
        return "LowMainVoltageAlarmCount";
    if (VEDIRECT_H12 == label)
        return "HighMainVoltageAlarmCount";
    if (VEDIRECT_H15 == label)
        return "MinAuxBatteryVoltage";
    if (VEDIRECT_H16 == label)
        return "MaxAuxBatteryVoltage";
    if (VEDIRECT_H17 == label)
        return "AmountDischargedEnergyKWh";
    if (VEDIRECT_H18 == label)
        return "AmountChargedEnergyKWh";
    if (VEDIRECT_H19 == label)
        return "YieldTotalKWh";
    if (VEDIRECT_H20 == label)
        return "YieldTodayKWh";
    if (VEDIRECT_H21 == label)
        return "MaxPowerTodayWatts";
    if (VEDIRECT_H22 == label)
        return "YieldYesterdayKWh";
    if (VEDIRECT_H23 == label)
        return "MaximumPowerYesterdayWatts";
    if (VEDIRECT_ERR == label)
        return "ErrorCode";
    if (VEDIRECT_CS == label)
        return "StateOfOperation";
    if (VEDIRECT_PID == label)
        return "ProductId";
    if (VEDIRECT_SER == label)
        return "SerialNumber";
    if (VEDIRECT_HSDS == label)
        return "DaySequenceNumber";
    if (VEDIRECT_MODE == label)
        return "DeviceMode";
    if (VEDIRECT_RELAY == label)
        return "RelayState";
    return label;
}

const char *VictronBmv::parse_value(char *label, char *value) {

    if (VEDIRECT_V == label) {
        // mV to V
        char *buff;
        double val = std::strtof(value, &buff) / 1000;
        sprintf(buff, "%.4f", val);
        return buff;
    }

    if (VEDIRECT_I == label) {
        // mA to A
        char *buff;
        double val = std::strtof(value, &buff) / 1000;
        sprintf(buff, "%.4f", val);
        return buff;
    }

    if (VEDIRECT_SOC == label) {
        char *buff;
        double val = std::strtof(value, &buff) / 10;
        sprintf(buff, "%.1f", val);
        return buff;
    }

    if (VEDIRECT_H1 == label) {
        char *buff;
        double val = std::strtof(value, &buff) / 1000;
        sprintf(buff, "%.2f", val);
        return buff;
    }

    if (VEDIRECT_H2 == label) {
        char *buff;
        double val = std::strtof(value, &buff) / 1000;
        sprintf(buff, "%.2f", val);
        return buff;
    }

    if (VEDIRECT_H6 == label) {
        char *buff;
        double val = std::strtof(value, &buff) / 100;
        sprintf(buff, "%.2f", val);
        return buff;
    }

    if (VEDIRECT_H7 == label) {
        // mV to V
        char *buff;
        double val = std::strtof(value, &buff) / 1000;
        sprintf(buff, "%.4f", val);
        return buff;
    }

    if (VEDIRECT_H8 == label) {
        // mV to V
        char *buff;
        double val = std::strtof(value, &buff) / 1000;
        sprintf(buff, "%.4f", val);
        return buff;
    }

    return value;
}

