//
// Created by bgrobler on 7/16/19.
//

#ifndef GRIT_PROBE_TSMPPT60_H
#define GRIT_PROBE_TSMPPT60_H


static const int V_PU_HI = 0x0000;

static const int V_PU_LO = 0x0001;

static const int I_PU_HI = 0x0002;

static const int I_PU_LO = 0x0003;

static const int VER_SW = 0x0004;

static const int ADC_VB_F_MED = 0x0018;

static const int ADC_VB_TERM = 0x0019;

static const int ADC_VBS_F = 0x001A;

static const int ADC_VA_F = 0x001B;

static const int ADC_IB_F_SHADOW = 0x001C;

static const int ADC_IA_F_SHADOW = 0x001D;

static const int ADC_P12_F = 0x001E;

static const int ADC_P3_F = 0x001F;

static const int ADC_PMETER_F = 0x0020;

static const int ADC_P18_F = 0x0021;

static const int ADC_V_REF = 0x0022;

static const int T_HS = 0x0023;

static const int T_RTS = 0x0024;

static const int T_BATT = 0x0025;

static const int ADC_VB_F_1M = 0x0026;

static const int ADC_IB_F_1M = 0x0027;

static const int LED_STATE = 0x0031;

static const int CHARGE_STATE = 0x0032;

static const int POWER_OUT_SHADOW = 0x003A;

static const int POWER_IN_SHADOW = 0x003B;

static const int SWEEP_PIN_MAX = 0x003C;

static const int SWEEP_VMP = 0x003D;

static const int SWEEP_VOC = 0x003E;

#include "../util/config.h"
#include "../../lib/modbuspp/modbus.h"
#include "../mq/mqtt_client.h"
#include <thread>

class TsMppt60 {

private:

    Config *config;

    void poll();
    modbus *mb_conn;

public:
    ~TsMppt60();

    void start() {

        if (this->config && this->config->getTsMppt60Enabled()) {
            if (!isRunning) {
                isRunning = true;
                this->task = std::thread(&TsMppt60::poll, this);
            }
        }
    }

    TsMppt60(Config *config, MqttClient *mqtt_client);


    static const int LED_START = 0;
    static const int LED_START_2 = 1;
    static const int LED_BRANCH = 2;
    static const int LED_FAST_GREEN_BLINK = 3;
    static const int LED_SLOW_GREEN_BLINK = 4;
    static const int LED_GREEN_BLINK_1HZ = 5;
    static const int LED_GREEN_LED = 6;
    static const int LED_UNDEFINED_1 = 7;
    static const int LED_YELLOW_LED = 8;
    static const int LED_UNDEFINED_2 = 9;
    static const int LED_RED_LED_BLINK = 10;
    static const int LED_RED_LED = 11;
    static const int LED_R_Y_G_ERROR = 12;
    static const int LED_RY_G_ERROR = 13;
    static const int LED_RG_Y_ERROR = 14;
    static const int LED_R_Y_ERROR_HTD = 15;
    static const int LED_R_G_ERROR_HVD = 16;
    static const int LED_RY_GY_ERROR = 17;
    static const int LED_GYR_ERROR = 18;
    static const int LED_GYR2 = 19;

    void publish(string software_version, string battery_voltage_filtered, string battery_terminal_voltage,
                 string battery_sense_voltage, string array_voltage_filtered,
                 string battery_current_filtered, string array_current_filtered, string power_supply_12v,
                 string power_supply_3v, string meterbus_voltage_filtered,
                 string power_supply_1_8v, string reference_voltage, string heatsink_temperature_celsius,
                 string rts_termperature_celsius, string battery_temperature_celsius,
                 string battery_voltage_1_min, string charging_current_1_min, string led_state_indicator,
                 string charging_state,
                 string mppt_output_power, string mppt_input_power, string mppt_max_power_sweep, string mppt_vmp_sweep,
                 string mppt_voc_sweep);

    MqttClient *mqtt_client;

    void connect(Config *config);

    const char *get_led_state_display(const char *led_state);


    string get_charging_state_display(string charging_state);

    bool isRunning = false;
    thread task;
};

#endif //GRIT_PROBE_TSMPPT60_H
