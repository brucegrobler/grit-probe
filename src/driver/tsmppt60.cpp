//
// Created by bgrobler on 7/16/19.
//

#include "tsmppt60.h"
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <syslog.h>

#include "../../lib/modbuspp/modbus.h"
#include "../../lib/modbuspp/modbus_exception.h"
#include "../mq/mqtt_client.h"

TsMppt60::TsMppt60(Config *config, MqttClient *mqtt_client) {

    if (config->getTsMppt60Enabled()) {

        this->config = config;
        this->mqtt_client = mqtt_client;
        this->mb_conn = new modbus(config->getTsMppt60Hostname(), config->getTsMppt60Port());
        this->mb_conn->modbus_set_slave_id(1);
    }
}

void TsMppt60::connect(Config *config) {
    syslog(LOG_DEBUG, "TSMPPT60 Connecting...");

    try {

        mb_conn->modbus_connect();
        syslog(LOG_DEBUG, "TSMPPT60 Connected...");
    } catch (const std::runtime_error &re) {
        std::cerr << "TSMPPT60: Init Runtime error: " << re.what() << std::endl;
        syslog(LOG_ERR, "TSMPPT60: Runtime Error: %s", re.what());
        mb_conn->modbus_close();

    }
    catch (const std::exception &ex) {
        std::cerr << "TSMPPT60: Init Error occurred: " << ex.what() << std::endl;
        syslog(LOG_ERR, "TSMPPT60: Error: %s", ex.what());
        mb_conn->modbus_close();
    }
    catch (...) {
        // catch any other errors (that we have no information about)
        std::cerr << "TSMPPT60: Init Unknown failure occurred. Possible memory corruption" << std::endl;
        syslog(LOG_ERR, "TSMPPT60: Unknown failure occurred. Possible memory corruption");
        mb_conn->modbus_close();
    }
}

void TsMppt60::poll() {

    while (isRunning) {
        try {
            connect(config);

            uint16_t v_pu_hi[1];
            mb_conn->modbus_read_holding_registers(V_PU_HI, 1, v_pu_hi);


            uint16_t v_pu_lo[1];
            mb_conn->modbus_read_holding_registers(V_PU_LO, 1, v_pu_lo);

            // V Scaling
            uint v_pu = v_pu_hi[0] + v_pu_lo[1] / pow(2, 16);

            uint16_t i_pu_hi[1];
            mb_conn->modbus_read_holding_registers(I_PU_HI, 1, i_pu_hi);

            uint16_t i_pu_lo[1];
            mb_conn->modbus_read_holding_registers(I_PU_LO, 1, i_pu_lo);

            // I Scaling
            uint i_pu = i_pu_hi[0] + i_pu_lo[1] / pow(2, 16);

            if (v_pu && i_pu) {

                uint16_t ver_sw[1];
                mb_conn->modbus_read_holding_registers(VER_SW, 1, ver_sw);
                string software_version = std::to_string(ver_sw[0]).c_str();


                uint16_t adc_vb_f_med[1];
                mb_conn->modbus_read_holding_registers(ADC_VB_F_MED, 1, adc_vb_f_med);
                string battery_voltage_filtered = std::to_string(adc_vb_f_med[0] * v_pu * pow(2, -15)).c_str();


                uint16_t adc_vbterm_f[1];
                mb_conn->modbus_read_holding_registers(ADC_VB_TERM, 1, adc_vbterm_f);
                string battery_terminal_voltage = std::to_string(adc_vbterm_f[0] * v_pu * pow(2, -15)).c_str();

                uint16_t adc_vbs_f[1];
                mb_conn->modbus_read_holding_registers(ADC_VBS_F, 1, adc_vbs_f);
                string battery_sense_voltage = std::to_string(adc_vbs_f[0] * v_pu * pow(2, -15)).c_str();

                uint16_t adc_va_f[1];
                mb_conn->modbus_read_holding_registers(ADC_VA_F, 1, adc_va_f);
                string array_voltage_filtered = std::to_string(adc_va_f[0] * v_pu * pow(2, -15)).c_str();

                uint16_t adc_ib_f_shadow[1];
                mb_conn->modbus_read_holding_registers(ADC_IB_F_SHADOW, 1, adc_ib_f_shadow);
                string battery_current_filtered = std::to_string(adc_ib_f_shadow[0] * i_pu * pow(2, -15)).c_str();

                uint16_t adc_ia_f_shadow[1];
                mb_conn->modbus_read_holding_registers(ADC_IA_F_SHADOW, 1, adc_ia_f_shadow);
                string array_current_filtered = std::to_string(adc_ia_f_shadow[0] * i_pu * pow(2, -15)).c_str();

                uint16_t adc_p12_f[1];
                mb_conn->modbus_read_holding_registers(ADC_P12_F, 1, adc_p12_f);
                string power_supply_12v = std::to_string(adc_p12_f[0] * 18.612 * pow(2, -15)).c_str();

                uint16_t adc_p3_f[1];
                mb_conn->modbus_read_holding_registers(ADC_P3_F, 1, adc_p3_f);
                string power_supply_3v = std::to_string(adc_p3_f[0] * 6.6 * pow(2, -15)).c_str();

                uint16_t adc_pmeter_f[1];
                mb_conn->modbus_read_holding_registers(ADC_PMETER_F, 1, adc_pmeter_f);
                string meterbus_voltage_filtered = std::to_string(adc_pmeter_f[0] * 18.612 * pow(2, -15)).c_str();

                uint16_t adc_p18_f[1];
                mb_conn->modbus_read_holding_registers(ADC_P18_F, 1, adc_p18_f);
                string power_supply_1_8v = std::to_string(adc_p18_f[0] * 3.2 * pow(2, -15)).c_str();

                uint16_t adc_v_ref[1];
                mb_conn->modbus_read_holding_registers(ADC_V_REF, 1, adc_v_ref);
                string reference_voltage = std::to_string(adc_v_ref[0] * 3.2 * pow(2, -15)).c_str();

                uint16_t t_hs[1];
                mb_conn->modbus_read_holding_registers(T_HS, 1, t_hs);
                string heatsink_temperature_celsius = std::to_string(t_hs[0]).c_str();

                uint16_t t_rts[1];
                mb_conn->modbus_read_holding_registers(T_RTS, 1, t_rts);
                string rts_temperature_celsius = std::to_string(t_rts[0]).c_str();

                uint16_t t_batt[1];
                mb_conn->modbus_read_holding_registers(T_BATT, 1, t_batt);
                string battery_temperature_celsius = std::to_string(t_batt[0]).c_str();

                uint16_t adc_vb_f_1m[1];
                mb_conn->modbus_read_holding_registers(ADC_VB_F_1M, 1, adc_vb_f_1m);
                string battery_voltage_1_min = std::to_string(adc_vb_f_1m[0] * v_pu * pow(2, -15)).c_str();

                uint16_t adc_ib_f_1m[1];
                mb_conn->modbus_read_holding_registers(ADC_IB_F_1M, 1, adc_ib_f_1m);
                string charging_current_1_min = std::to_string(adc_ib_f_1m[0] * i_pu * pow(2, -15)).c_str();


                uint16_t led_state[1];
                mb_conn->modbus_read_holding_registers(LED_STATE, 1, led_state);
                string led_state_indicator = std::to_string(led_state[0]).c_str();

                uint16_t charge_state[1];
                mb_conn->modbus_read_holding_registers(CHARGE_STATE, 1, charge_state);
                string charging_state = std::to_string(charge_state[0]).c_str();

                // MPPT
                uint16_t power_out_shadow[1];
                mb_conn->modbus_read_holding_registers(POWER_OUT_SHADOW, 1, power_out_shadow);
                string mppt_output_power = std::to_string(power_out_shadow[0] * v_pu * i_pu * pow(2, -17)).c_str();

                uint16_t power_in_shadow[1];
                mb_conn->modbus_read_holding_registers(POWER_IN_SHADOW, 1, power_in_shadow);
                string mppt_input_power = std::to_string(power_in_shadow[0] * v_pu * i_pu * pow(2, -17)).c_str();

                uint16_t sweep_pin_max[1];
                mb_conn->modbus_read_holding_registers(SWEEP_PIN_MAX, 1, sweep_pin_max);
                string mppt_max_power_sweep = std::to_string(sweep_pin_max[0] * v_pu * i_pu * pow(2, -17)).c_str();

                uint16_t sweep_vmp[1];
                mb_conn->modbus_read_holding_registers(SWEEP_VMP, 1, sweep_vmp);
                string mppt_vmp_sweep = std::to_string(sweep_vmp[0] * v_pu * pow(2, -15)).c_str();

                uint16_t sweep_voc[1];
                mb_conn->modbus_read_holding_registers(SWEEP_VOC, 1, sweep_voc);
                string mppt_voc_sweep = std::to_string(sweep_voc[0] * v_pu * pow(2, -15)).c_str();

                publish(software_version, battery_voltage_filtered, battery_terminal_voltage,
                        battery_sense_voltage, array_voltage_filtered, battery_current_filtered,
                        array_current_filtered, power_supply_12v, power_supply_3v, meterbus_voltage_filtered,
                        power_supply_1_8v, reference_voltage, heatsink_temperature_celsius, rts_temperature_celsius,
                        battery_temperature_celsius, battery_voltage_1_min, charging_current_1_min, led_state_indicator,
                        charging_state, mppt_output_power, mppt_input_power, mppt_max_power_sweep, mppt_vmp_sweep,
                        mppt_voc_sweep);
            }

        } catch (const std::runtime_error &re) {
            std::cerr << "TSMPPT60: Runtime error: " << re.what() << std::endl;
            syslog(LOG_ERR, "TSMPPT60: Runtime Error: %s", re.what());
            mb_conn->modbus_close();

        }
        catch (const std::exception &ex) {
            std::cerr << "TSMPPT60: Error occurred: " << ex.what() << std::endl;
            syslog(LOG_ERR, "TSMPPT60: Error: %s", ex.what());
            mb_conn->modbus_close();
        }
        catch (...) {
            // catch any other errors (that we have no information about)
            std::cerr << "TSMPPT60: Unknown failure occurred. Possible memory corruption" << std::endl;
            syslog(LOG_ERR, "TSMPPT60: Unknown failure occurred. Possible memory corruption");
            mb_conn->modbus_close();
        }

        mb_conn->modbus_close();
        sleep(config->getTsMppt60PollInterval());
    }
}

TsMppt60::~TsMppt60() {

    mb_conn->modbus_close();

}

void
TsMppt60::publish(string software_version, string battery_voltage_filtered, string battery_terminal_voltage,
                  string battery_sense_voltage, string array_voltage_filtered,
                  string battery_current_filtered, string array_current_filtered, string power_supply_12v,
                  string power_supply_3v, string meterbus_voltage_filtered,
                  string power_supply_1_8v, string reference_voltage, string heatsink_temperature_celsius,
                  string rts_termperature_celsius, string battery_temperature_celsius,
                  string battery_voltage_1_min, string charging_current_1_min, string led_state_indicator,
                  string charging_state,
                  string mppt_output_power, string mppt_input_power, string mppt_max_power_sweep, string mppt_vmp_sweep,
                  string mppt_voc_sweep) {

    // TODO Make all the topic config...

    mqtt_client->publish(NULL, "morningstar/tsmppt60/SoftwareVersion", strlen(software_version.c_str()),
                         software_version.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/BatteryVoltsFiltered", strlen(battery_voltage_filtered.c_str()),
                         battery_voltage_filtered.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/BatteryVoltsTerminal", strlen(battery_terminal_voltage.c_str()),
                         battery_terminal_voltage.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/BatterySenseVolts", strlen(battery_sense_voltage.c_str()),
                         battery_sense_voltage.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/ArrayVoltsFiltered", strlen(array_voltage_filtered.c_str()),
                         array_voltage_filtered.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/BatteryCurrentFiltered", strlen(battery_current_filtered.c_str()),
                         array_current_filtered.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/PowerSupply12Volts", strlen(power_supply_12v.c_str()),
                         power_supply_12v.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/PowerSupply3Volts", strlen(power_supply_3v.c_str()),
                         power_supply_3v.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/MetereBusVolts", strlen(meterbus_voltage_filtered.c_str()),
                         meterbus_voltage_filtered.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/PowerSupply1-8Volts", strlen(power_supply_1_8v.c_str()),
                         power_supply_1_8v.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/ReferenceVoltage", strlen(reference_voltage.c_str()),
                         reference_voltage.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/HeatsinkTemperature_Celsius",
                         strlen(heatsink_temperature_celsius.c_str()), heatsink_temperature_celsius.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/RTSTemperatureCelsius", strlen(rts_termperature_celsius.c_str()),
                         rts_termperature_celsius.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/BatteryTemperatureCelsius",
                         strlen(battery_temperature_celsius.c_str()), battery_temperature_celsius.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/BatteryVolts1min", strlen(battery_voltage_1_min.c_str()),
                         battery_voltage_1_min.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/ChargingCurrent1min", strlen(charging_current_1_min.c_str()),
                         charging_current_1_min.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/LedState", strlen(led_state_indicator.c_str()),
                         led_state_indicator.c_str());
    // TODO BG - Make this return friendly string.
//    mqtt_client->publish(NULL, "morningstar/tsmppt60/LedStateDisplay", strlen(led_state_indicator.c_str()),
//                         getLedStateDisplay(led_state_indicator.c_str()));
    mqtt_client->publish(NULL, "morningstar/tsmppt60/ChargingState", strlen(charging_state.c_str()),
                         charging_state.c_str());

    string charging_state_display = get_charging_state_display(charging_state);
    mqtt_client->publish(NULL, "morningstar/tsmppt60/ChargingStateDisplay", strlen(charging_state_display.c_str()),
                         charging_state_display.c_str());

    mqtt_client->publish(NULL, "morningstar/tsmppt60/MpptOutputPower", strlen(mppt_output_power.c_str()),
                         mppt_output_power.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/MpptInputPower", strlen(mppt_input_power.c_str()),
                         mppt_input_power.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/MpptMaxPowerSweep", strlen(mppt_max_power_sweep.c_str()),
                         mppt_max_power_sweep.c_str());
    mqtt_client->publish(NULL, "morningstar/tsmppt60/MpptVmpSweep", strlen(mppt_vmp_sweep.c_str()),
                         mppt_vmp_sweep.c_str());

    mqtt_client->publish(NULL, "morningstar/tsmppt60/MpptVocSweep", strlen(mppt_voc_sweep.c_str()),
                         mppt_voc_sweep.c_str());


}

string TsMppt60::get_charging_state_display(string charging_state) {

    int charging_state_int = atoi(charging_state.c_str());

    if (charging_state_int == 0) {
        return "START";
    }
    if (charging_state_int == 1) {
        return "NIGHT_CHECK";
    }
    if (charging_state_int == 2) {
        return "DISCONNECT";
    }
    if (charging_state_int == 3) {
        return "NIGHT";
    }
    if (charging_state_int == 4) {
        return "FAULT";
    }
    if (charging_state_int == 5) {
        return "MPPT";
    }
    if (charging_state_int == 6) {
        return "ABSORPTION";
    }
    if (charging_state_int == 7) {
        return "FLOAT";
    }
    if (charging_state_int == 8) {
        return "EQUALIZE";
    }
    if (charging_state_int == 9) {
        return "SLAVE";
    }
    return "UNKNOWN";

}



