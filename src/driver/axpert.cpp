#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <syslog.h>
#include <cstring>
#include <iostream>
#include "axpert.h"
#include "../main.h"

#include "../util/config.h"
#include "../util/utils.h"

Voltronic::Voltronic(Config *config, MqttClient *mqtt_client) {

    this->config = config;
    this->mqtt_client = mqtt_client;

    if (config->getAxpertEnabled()) {
        status[0] = 0;
        mode = 0;
    }

}

string *Voltronic::GetStatus() {
    m.lock();
    string *result = new string(status);
    m.unlock();
    return result;
}

void Voltronic::SetMode(char newmode) {
    m.lock();
    mode = newmode;
    m.unlock();
}

string *Voltronic::GetMode() {
    string *result;
    m.lock();
    switch (mode) {
        case 'P':
            result = new string("Power On Mode");
            break;
        case 'S':
            result = new string("Standby Mode");
            break;
        case 'L':
            result = new string("Line Mode");
            break;
        case 'B':
            result = new string("Battery Mode");
            break;
        case 'F':
            result = new string("Fault Mode");
            break;
        case 'H':
            result = new string("Power Saving Mode");
            break;
        default:
            result = new string("Unknown");
            break;
    }
    m.unlock();
    return result;
}


uint16_t Voltronic::cal_crc_half(uint8_t *data, uint8_t len) {
    uint16_t crc;

    uint8_t da;
    uint8_t *ptr;
    uint8_t bCRCHign;
    uint8_t bCRCLow;

    uint16_t crc_ta[16] =
            {
                    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
                    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
            };
    ptr = data;
    crc = 0;

    while (len-- != 0) {
        da = ((uint8_t) (crc >> 8)) >> 4;
        crc <<= 4;
        crc ^= crc_ta[da ^ (*ptr >> 4)];
        da = ((uint8_t) (crc >> 8)) >> 4;
        crc <<= 4;
        crc ^= crc_ta[da ^ (*ptr & 0x0f)];
        ptr++;
    }
    bCRCLow = crc;
    bCRCHign = (uint8_t) (crc >> 8);
    if (bCRCLow == 0x28 || bCRCLow == 0x0d || bCRCLow == 0x0a)
        bCRCLow++;
    if (bCRCHign == 0x28 || bCRCHign == 0x0d || bCRCHign == 0x0a)
        bCRCHign++;
    crc = ((uint16_t) bCRCHign) << 8;
    crc += bCRCLow;
    return (crc);
}


bool Voltronic::is_valid_crc(unsigned char *data, int len) {
    uint16_t crc = cal_crc_half(data, len - 3);
    return data[len - 3] == (crc >> 8) && data[len - 2] == (crc & 0xff);
}

bool Voltronic::query(const char *cmd, int reply_size) {
    time_t started;
    int fd;
    int i = 0, n;

    fd = open(config->getAxpertPortFilename(), O_RDWR | O_NONBLOCK);
    if (fd == -1) {
        syslog(LOG_NOTICE, "Axpert: Unable to open device file [%s] (errno=%d %s)\n", config->getAxpertPortFilename(),
               errno,
               strerror(errno));
        sleep(5);
        return false;
    }

    //generating CRC for a command
    uint16_t crc = cal_crc_half((uint8_t *) cmd, strlen(cmd));
    n = strlen(cmd);
    memcpy(&buffer, cmd, n);
    buffer[n++] = crc >> 8;
    buffer[n++] = crc & 0xff;
    buffer[n++] = 0x0d;

    for (int i = 0; i < n; ++i)
        std::cout << std::hex << (int) buffer[i];

    //send a command
    write(fd, &buffer, n);
    time(&started);

    do {
        n = read(fd, (void *) buffer + i, reply_size - i);
        if (n < 0) {
            if (time(nullptr) - started > 2) {
                syslog(LOG_NOTICE, "Axpert: %s read timeout\n", cmd);
                break;
            } else {
                usleep(10);
                continue;
            }
        }
        i += n;
    } while (i < reply_size);
    close(fd);

    if (i == reply_size) {
        if (buffer[0] != '(' || buffer[reply_size - 1] != 0x0d) {
            syslog(LOG_NOTICE, "Axpert: %s: incorrect start/stop bytes\n", cmd);
            return false;
        }
        if (!(is_valid_crc(buffer, reply_size))) {
            syslog(LOG_NOTICE, "Axpert: %s: CRC Failed\n", cmd);
            return false;
        }
        buffer[i - 3] = '\0'; //nullptr terminating on first CRC byte
        syslog(LOG_NOTICE, "Axpert: %s: %d bytes read: %s\n", cmd, i, buffer);
        return true;
    } else
        syslog(LOG_NOTICE, "Axpert: %s reply too short (%d bytes)\n", cmd, i);
    return false;
}

void Voltronic::poll() {

    while (isRunning) {

        try {
            if (query("QPIGS", 110)) {
                strcpy(status, (const char *) buffer + 1);
                publishQpigs();
            }

            // reading mode (QMOD)
            if (query("QMOD", 5)) {
                char tempMode[1024];
                strcpy(tempMode, (const char *) buffer + 1);
                SetMode(tempMode[0]);
                publishQmod();
            }

            if (query("PCP00", 7)) {
                strcpy(status, (const char *) buffer + 1);
            }

            sleep(config->getAxpertQpigsPollInterval());
        } catch (const std::runtime_error &re) {
            std::cerr << "Voltronic: Runtime error: " << re.what() << std::endl;
            syslog(LOG_ERR, "Voltronic: Runtime Error: %s", re.what());


        }
        catch (const std::exception &ex) {
            std::cerr << "Voltronic: Error occurred: " << ex.what() << std::endl;
            syslog(LOG_ERR, "Voltronic: Error: %s", ex.what());

        }
        catch (...) {
            // catch any other errors (that we have no information about)
            std::cerr << "Voltronic: Unknown failure occurred. Possible memory corruption" << std::endl;
            syslog(LOG_ERR, "Voltronic: Unknown failure occurred. Possible memory corruption");

        }

    }
}


void Voltronic::publishQpigs() {

    string *reply = GetStatus();

    if (reply) {
        string temp_status = (reply)->c_str();
        string data = (temp_status.substr(0, strlen(temp_status.c_str())));

        Qpigs *qpigs = new Qpigs((reply)->c_str());

        mqtt_client->publish(nullptr,
                             config->getMqttAxpertInverterLoadVoltsTopic().c_str(),
                             strlen(qpigs->get_load_voltage().c_str()),
                             qpigs->get_load_voltage().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterLoadFreqTopic().c_str(),
                             strlen(qpigs->get_load_frequency().c_str()),
                             qpigs->get_load_frequency().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterLoadPercentTopic().c_str(),
                             strlen(qpigs->get_load_percent().c_str()),
                             qpigs->get_load_percent().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterLoadWattTopic().c_str(),
                             strlen(qpigs->get_load_power().c_str()),
                             qpigs->get_load_power().c_str());

        mqtt_client->publish(nullptr, "LoadPowerRealEst",
                             strlen(qpigs->get_est_load_power().c_str()),
                             qpigs->get_est_load_power().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterBattVoltsTopic().c_str(),
                             strlen(qpigs->get_battery_voltage().c_str()),
                             qpigs->get_battery_voltage().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterBattAmpsTopic().c_str(),
                             strlen(qpigs->get_battery_current().c_str()),
                             qpigs->get_battery_current().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterBattWattsTopic().c_str(),
                             strlen(qpigs->get_battery_power().c_str()),
                             qpigs->get_battery_power().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterBattSocTopic().c_str(),
                             strlen(qpigs->get_battery_capacity().c_str()),
                             qpigs->get_battery_capacity().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterTempTopic().c_str(),
                             strlen(qpigs->get_heatsink_temperature().c_str()),
                             qpigs->get_heatsink_temperature().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterGridVoltsTopic().c_str(),
                             strlen(qpigs->get_grid_voltage().c_str()),
                             qpigs->get_grid_voltage().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterGridFreqTopic().c_str(),
                             strlen(qpigs->get_grid_frequency().c_str()),
                             qpigs->get_grid_frequency().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterGridWattsTopic().c_str(),
                             strlen(qpigs->get_grid_power().c_str()),
                             qpigs->get_grid_power().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterMpptVolts().c_str(),
                             strlen(qpigs->get_mppt_voltage_1().c_str()),
                             qpigs->get_mppt_voltage_1().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterMpptWatts().c_str(),
                             strlen(qpigs->get_mppt_power_1().c_str()),
                             qpigs->get_mppt_power_1().c_str());

        mqtt_client->publish(nullptr, config->getMqttAxpertInverterMpptCurrent().c_str(),
                             strlen(qpigs->get_mppt_current_1().c_str()),
                             qpigs->get_mppt_current_1().c_str());

        delete reply;
    }
}

void Voltronic::publishQmod() {

    mqtt_client->publish(nullptr, config->getMqttAxpertInverterModeTopic().c_str(),
                         strlen(GetMode()->c_str()),
                         GetMode()->c_str());


}

Qpigs::Qpigs(const char *data) {

//    Device: (N1N2N3.N5 N7N 8.N10 N12N13N14.N 16 N18N19.N21 N23N24N25N26 N28N29N30 N31 N33N34N35
//    N37 N38N39 N41N42.N44N45 N47N48N49 N51N52N53 N55N56N57 N58 N60N61N62N 63 N65N66N67.N69
//    N71 N72.N74N75 N77N 78N79N80N81 b83 b84b85b86b87b88b89b90 N92N93 N95N96 N98N99N100N101N102
//    b104b 105b106 <CRC><cr>

//    217.6 50.0 217.6 50.0
//    0413 0338 008 472
//    58.40 018 095 0067
//    0000 000.0 00.00 00000
//    00010101 00 00 00000
//    010

    if (strlen(data) == 106) {

        char buffer[106];
        strcpy(buffer, data);

        float grid_voltage_float = atof(std::strtok(buffer, delimeter));

        this->grid_voltage = to_string(grid_voltage_float);
        this->grid_frequency = to_string(atof(std::strtok(nullptr, delimeter)));
        this->load_voltage = to_string(atof(std::strtok(nullptr, delimeter)));
        this->load_frequency = to_string(atof(std::strtok(nullptr, delimeter)));

        this->load_apparent_power = to_string(atoi(std::strtok(nullptr, delimeter)));

        int load_power_int = atoi(std::strtok(nullptr, delimeter));
        this->load_power = to_string(load_power_int);
        this->load_percent = to_string(atof(std::strtok(nullptr, delimeter)));
        this->bus_dc_voltage = to_string(atoi(std::strtok(nullptr, delimeter)));

        float battery_voltage_float = atof(std::strtok(nullptr, delimeter));
        this->battery_voltage = to_string(battery_voltage_float);

        int batt_charge_current_int = atoi(std::strtok(nullptr, delimeter));
        this->battery_charge_current = to_string(batt_charge_current_int);
        this->battery_capacity = to_string(atoi(std::strtok(nullptr, delimeter)));
        this->heatsink_temperature = to_string(atoi(std::strtok(nullptr, delimeter)));

        this->mppt_current_1 = to_string(atoi(std::strtok(nullptr, delimeter)));
        this->mppt_voltage_1 = to_string(atof(std::strtok(nullptr, delimeter)));
        this->battery_voltage_scc_1 = to_string(atof(std::strtok(nullptr, delimeter)));

        int batt_discharge_current_int = atoi(std::strtok(nullptr, delimeter));
        this->battery_discharge_current = to_string(batt_discharge_current_int);

        this->battery_current = to_string(batt_charge_current_int + batt_discharge_current_int);

        this->battery_power = to_string((batt_charge_current_int + batt_discharge_current_int) * battery_voltage_float);

        this->device_status_1 = std::strtok(nullptr, delimeter);
        this->sbu_priority_version = device_status_1.substr(0, 1);
        this->configuration_updated = device_status_1.substr(1, 2);
        this->scc_firmware_updated = device_status_1.substr(2, 3);
        this->load_status = device_status_1.substr(3, 4);
        this->battery_voltage_steady_under_charge = device_status_1.substr(4, 5);
        this->device_charging_status = device_status_1.substr(5, 7);

        if (device_charging_status == "110") {
            this->charging_status_scc_1 = "1";
        }
        if (device_charging_status == "101") {
            this->ac_charge_enabled = "1";
        }

        this->battery_voltage_offset = to_string(atoi(std::strtok(nullptr, delimeter)));
        this->eeprom_version = std::strtok(nullptr, delimeter);
        const int mppt_power_1_int = atoi(std::strtok(nullptr, delimeter));
        this->mppt_power_1 = to_string(mppt_power_1_int);

        this->device_status_2 = std::strtok(nullptr, delimeter);

        this->charging_float_mode = device_status_2.substr(0, 1);
        this->power_switch_state = device_status_2.substr(1, 2);
        this->reserved_1 = device_status_2.substr(2, 3);

        if (grid_voltage_float > 0) {
            this->grid_power = to_string(
                    (batt_charge_current_int * battery_voltage_float) + load_power_int - mppt_power_1_int);
        } else {
            this->grid_power = to_string(0);
        }

        syslog(LOG_DEBUG, "Voltronic: Qpigs - Parse complete.");
    } else {
        syslog(LOG_WARNING, "Voltronic: Qpigs - Cannot Parse [%s]", data);
    }
}

string Qpigs::get_load_voltage() {
    return load_voltage;
}

string Qpigs::get_load_frequency() {
    return load_frequency;
}

string Qpigs::get_load_percent() {
    return load_percent;
}

string Qpigs::get_load_power() {
    return load_power;
}

string Qpigs::get_battery_voltage() {
    return battery_voltage;
}

string Qpigs::get_battery_capacity() {
    return battery_capacity;
}

string Qpigs::get_heatsink_temperature() {
    return heatsink_temperature;
}

string Qpigs::get_grid_voltage() {
    return grid_voltage;
}

string Qpigs::get_grid_frequency() {
    return grid_frequency;
}

string Qpigs::get_mppt_voltage_1() {
    return mppt_voltage_1;
}

string Qpigs::get_mppt_power_1() {
    return mppt_power_1;
}

string Qpigs::get_mppt_current_1() {
    return mppt_current_1;
}

string Qpigs::get_configuration_updated() {
    return configuration_updated;
}

string Qpigs::get_reserved_1() {
    return reserved_1;
}

string Qpigs::get_sbu_priority_version() {
    return sbu_priority_version;
}

string Qpigs::get_battery_discharge_current() {
    return battery_discharge_current;
}

string Qpigs::get_battery_voltage_scc_1() {
    return battery_voltage_scc_1;
}

string Qpigs::get_battery_charge_current() {
    return battery_charge_current;
}

string Qpigs::get_bus_dc_voltage() {
    return bus_dc_voltage;
}

string Qpigs::get_load_apparent_power() {
    return load_apparent_power;
}

string Qpigs::get_power_switch_state() {
    return power_switch_state;
}

string Qpigs::get_charging_float_mode() {
    return charging_float_mode;
}

string Qpigs::get_eeprom_version() {
    return eeprom_version;
}

string Qpigs::get_battery_voltage_offset() {
    return battery_voltage_offset;
}

string Qpigs::get_charging_status() {
    return charging_status;
}

string Qpigs::get_ac_charge_enabled() {
    return ac_charge_enabled;
}

string Qpigs::get_charging_status_scc_1() {
    return charging_status_scc_1;
}

string Qpigs::get_battery_voltage_steady_under_charge() {
    return battery_voltage_steady_under_charge;
}

string Qpigs::get_load_status() {
    return load_status;
}

string Qpigs::get_scc_firmware_updated() {
    return scc_firmware_updated;
}

string Qpigs::get_battery_current() {
    return battery_current;
}

string Qpigs::get_battery_power() {
    return battery_power;
}

string Qpigs::get_grid_power() {
    return grid_power;
}

string Qpigs::get_est_load_power() {
    char *buff;
    int est_load = std::strtof(load_power.c_str(), &buff);

    return std::to_string(est_load + 60);

}
