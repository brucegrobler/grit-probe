//
// Created by bgrobler on 7/12/19.
//

#ifndef AXPERT_VICTRON_BMV_H
#define AXPERT_VICTRON_BMV_H

#include <thread>
#include <mutex>
#include <iostream>
#include "../util/config.h"
#include "../mq/mqtt_client.h"

#define LINE_LENGTH 20

static const int READ_CHUNKS = 3;

class VictronBmv {

public:

    VictronBmv(Config *config, MqttClient *mqtt_client);

    void start() {
        if (this->config && this->config->getVictronBmvEnabled()) {
            if (!isRunning) {
                isRunning = true;
                this->task = std::thread(&VictronBmv::poll, this);
            }
        }
    }

private:

    bool enabled;
    Config *config;

    MqttClient *mqtt_client;

    void poll();

    void serialRead();

    bool isRunning = false;

    std::thread task;

    void publish(const char *label, const char *value);

    const char *get_mapped_label(char *label);

    std::string VEDIRECT_V = "V";
    std::string VEDIRECT_T = "T";
    std::string VEDIRECT_I = "I";
    std::string VEDIRECT_P = "P";
    std::string VEDIRECT_CE = "CE";
    std::string VEDIRECT_SOC = "SOC";
    std::string VEDIRECT_TTG = "TTG";
    std::string VEDIRECT_ALARM = "Alarm";
    std::string VEDIRECT_BMV = "BMV";
    std::string VEDIRECT_FW = "FW";
    std::string VEDIRECT_H1 = "H1";
    std::string VEDIRECT_H2 = "H2";
    std::string VEDIRECT_H3 = "H3";
    std::string VEDIRECT_H4 = "H4";
    std::string VEDIRECT_H5 = "H5";
    std::string VEDIRECT_H6 = "H6";
    std::string VEDIRECT_H7 = "H7";
    std::string VEDIRECT_H8 = "H8";
    std::string VEDIRECT_H9 = "H9";
    std::string VEDIRECT_H10 = "H10";
    std::string VEDIRECT_H11 = "H11";
    std::string VEDIRECT_H12 = "H12";
    std::string VEDIRECT_H15 = "H15";
    std::string VEDIRECT_H16 = "H16";
    std::string VEDIRECT_H17 = "H17";
    std::string VEDIRECT_H18 = "H18";
    std::string VEDIRECT_H19 = "H19";
    std::string VEDIRECT_H20 = "H20";
    std::string VEDIRECT_H21 = "H21";
    std::string VEDIRECT_H22 = "H22";
    std::string VEDIRECT_H23 = "H23";
    std::string VEDIRECT_ERR = "ERR";
    std::string VEDIRECT_CS = "CS";
    std::string VEDIRECT_PID = "PID";
    std::string VEDIRECT_SER = "SER#";
    std::string VEDIRECT_HSDS = "HSDS";
    std::string VEDIRECT_MODE = "MODE";
    std::string VEDIRECT_RELAY = "Relay";

    const char *parse_value(char *label, char *value);

};


#endif //AXPERT_VICTRON_BMV_H
