//
// Created by bgrobler on 7/11/19.
//


#include <cstdio>
#include <stdlib.h>
#include <syslog.h>
#include <cstring>
#include "mqtt_client.h"

MqttClient::MqttClient(Config *config)
        : mosquittopp(config->getMqttClientId()) {

    this->config = config;
    if (config->getMqttEnabled()) {
        syslog(LOG_NOTICE, "MQTT: Connecting [%s:%s@%s:%d]", config->getMqttUsername(), config->getMqttPassword(),
               config->getMqttHostname(), config->getMqttPort());

        mosquittopp::username_pw_set(config->getMqttUsername(), config->getMqttPassword());
        mosqpp::lib_init();

        int keepalive = 120; // seconds

        mosquittopp::connect(config->getMqttHostname(), config->getMqttPort(), keepalive);

        mosquittopp::loop_start();
    } else {
        syslog(LOG_NOTICE, "MQTT: Client disabled. Check config grit:probe:mqtt:enabled = false");
    }
}

void MqttClient::on_connect(int rc) {

    if (rc == 5) {
        syslog(LOG_NOTICE, "MQTT: invalid username/password");
    }

    if (rc == 0) {
        syslog(LOG_NOTICE, "MQTT: Connected - status [%d]", rc);
        connected = true;
        subscribe(NULL, COMMAND_TOPIC);
    }
}

void MqttClient::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    syslog(LOG_NOTICE, "MQTT: Subscribed to [%s].", COMMAND_TOPIC);
}

void MqttClient::on_message(const struct mosquitto_message *message) {
    if (message && message->payload && message->topic)
        syslog(LOG_NOTICE, "MQTT: Received: [%s]:[%s]", message->topic, message->payload);
}

MqttClient::~MqttClient() {
    syslog(LOG_NOTICE, "MQTT:  Destroy...");
    mosquittopp::disconnect();
}


void MqttClient::publish(int *mid, const char *topic, int payloadlen, const void *payload) {

    if (connected) {
        mosquittopp::publish(mid, topic, payloadlen, payload);
    } else if (this->config->getMqttEnabled()) {
        syslog(LOG_NOTICE, "MQTT: Cannot Publish - disconnected");
    }

}

void MqttClient::publish(const char *topic, const char *payload) {

    publish(NULL, topic, strlen(payload), payload);

}
