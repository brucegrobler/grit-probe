//
// Created by bgrobler on 7/11/19.
//

#ifndef AXPERT_MQTT_CLIENT_H
#define AXPERT_MQTT_CLIENT_H


static const char *const COMMAND_TOPIC = "grit/probe/axpert/command";

#include <mosquittopp.h>
#include "../util/config.h"

class MqttClient : public mosqpp::mosquittopp {
public:

    MqttClient(Config *config);

    ~MqttClient();

    void on_connect(int rc);

    void on_message(const struct mosquitto_message *message);

    void on_subscribe(int /*mid*/, int /*qos_count*/, const int * /*granted_qos*/);

    void publish(int *mid, const char *topic, int payloadlen, const void *payload);

    void publish(const char *topic, const char *payload);

private:
    Config *config;

    bool connected = false;
};

#endif //AXPERT_MQTT_CLIENT_H
