//
// Created by bgrobler on 7/12/19.
//

#include "utils.h"
#include <cstdlib>
#include <string.h>
#include <libnet.h>
#include <ctime>
#include <stdarg.h>
#include <memory>

std::string Utils::current_date_time() {

    std::array<char, 64> buffer;
    buffer.fill(0);
    time_t rawtime;
    time(&rawtime);
    const auto timeinfo = localtime(&rawtime);
    strftime(buffer.data(), sizeof(buffer), "%Y-%m-%dT%H:%M:%S", timeinfo);

    return std::string(buffer.data());

}
