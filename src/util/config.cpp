//
// Created by bgrobler on 7/12/19.
//

#include <yaml-cpp/yaml.h>
#include <string.h>
#include <syslog.h>
#include "config.h"

Config::Config(const char *config_file) {

    /**
    * CONFIG
    */
    syslog(LOG_NOTICE, "Parsing config: %s", config_file);
    try {
        YAML::Node config = YAML::LoadFile(config_file);

        if (config && !config.IsNull()) {

            YAML::Node probe = config["grit"]["probe"];

            if (!probe.IsNull()) {

                YAML::Node mqtt = probe["mqtt"];
                if (!mqtt.IsNull()) {
                    try {
                        if (!mqtt["enabled"].IsNull()) {
                            mqtt_enabled = mqtt["enabled"].as<bool>();
                            if (mqtt_enabled) {
                                mqtt_hostname = mqtt["hostname"].as<std::string>();
                                mqtt_port = mqtt["port"].as<int>();
                                mqtt_username = mqtt["username"].as<std::string>();
                                mqtt_password = mqtt["password"].as<std::string>();
                                mqtt_client_id = mqtt["clientId"].as<std::string>();
                            }
                        }
                    } catch (YAML::BadConversion e) {
                        // TODO BG - figure out a better way - need to get line number that is the problem... e.mark causes segfault.
                        syslog(LOG_ERR, "WARNING - Config error [mqtt]: %s \n", e.msg.c_str());
                    }
                }

                YAML::Node axpert = probe["axpert"];
                if (!axpert.IsNull()) {
                    try {
                        if (!axpert["enabled"].IsNull()) {
                            axpert_enabled = axpert["enabled"].as<bool>();
                            if (axpert_enabled) {
                                axpert_port_filename = axpert["port"].as<std::string>();
                                axpert_qpigs_poll_interval = axpert["qpigsPollInterval"].as<int>();
                                axpert_mqtt_inverter_name_topic = axpert["data"]["topic"]["name"].as<std::string>();
                                axpert_mqtt_inverter_load_volts_topic = axpert["data"]["topic"]["loadVolts"].as<std::string>();
                                axpert_mqtt_inverter_load_freq_topic = axpert["data"]["topic"]["loadFreq"].as<std::string>();
                                axpert_mqtt_inverter_load_percent_topic = axpert["data"]["topic"]["loadPercent"].as<std::string>();
                                axpert_mqtt_inverter_load_watts_topic = axpert["data"]["topic"]["loadWatts"].as<std::string>();
                                axpert_mqtt_inverter_batt_volts_topic = axpert["data"]["topic"]["battVolts"].as<std::string>();
                                axpert_mqtt_inverter_batt_amps_topic = axpert["data"]["topic"]["battAmps"].as<std::string>();
                                axpert_mqtt_inverter_batt_watts_topic = axpert["data"]["topic"]["battWatts"].as<std::string>();
                                axpert_mqtt_inverter_batt_soc_topic = axpert["data"]["topic"]["battSoc"].as<std::string>();
                                axpert_mqtt_inverter_temp_celsius_topic = axpert["data"]["topic"]["tempCelsius"].as<std::string>();
                                axpert_mqtt_inverter_mode_topic = axpert["data"]["topic"]["mode"].as<std::string>();
                                axpert_mqtt_inverter_grid_volts_topic = axpert["data"]["topic"]["gridVolts"].as<std::string>();
                                axpert_mqtt_inverter_grid_watts_topic = axpert["data"]["topic"]["gridWatts"].as<std::string>();
                                axpert_mqtt_inverter_grid_freq_topic = axpert["data"]["topic"]["gridFreq"].as<std::string>();
                                axpert_mqtt_inverter_mppt_watts_topic = axpert["data"]["topic"]["mpptWatts"].as<std::string>();
                                axpert_mqtt_inverter_mppt_volts_topic = axpert["data"]["topic"]["mpptVolts"].as<std::string>();
                            }
                        }
                    } catch (YAML::BadConversion e) {
                        // TODO BG - figure out a better way - need to get line number that is the problem... e.mark causes segfault.
                        syslog(LOG_ERR, "WARNING - Config error [axpert]: %s \n", e.msg.c_str());
                    }
                }


                YAML::Node victron = probe["victron"];
                if (!victron.IsNull()) {
                    try {
                        victron_bmv_enabled = victron["bmv"]["enabled"].as<bool>();
                        if (victron_bmv_enabled) {
                            victron_bmv_port_filename = victron["bmv"]["port"].as<std::string>();
                            victron_bmv_poll_interval = victron["bmv"]["pollInterval"].as<int>();
                        }
                    } catch (YAML::BadConversion e) {
                        // TODO BG - figure out a better way - need to get line number that is the problem... e.mark causes segfault.
                        syslog(LOG_ERR, "WARNING - Config error [victron]: %s", e.msg.c_str());
                    }
                }

                YAML::Node morningstar = probe["morningstar"];
                if (!morningstar.IsNull()) {
                    try {
                        YAML::Node tsmppt60 = morningstar["tsmppt60"];
                        if (!morningstar["tsmppt60"].IsNull()) {
                            tsmppt60_enabled = tsmppt60["enabled"].as<bool>();
                            if (tsmppt60_enabled) {
                                tsmppt60_hostname = tsmppt60["hostname"].as<std::string>();
                                tsmppt60_port = tsmppt60["port"].as<int>();
                                tsmppt60_poll_interval = tsmppt60["pollInterval"].as<int>();
                            }
                        }
                    } catch (YAML::BadConversion e) {
                        // TODO BG - figure out a better way - need to get line number that is the problem... e.mark causes segfault.
                        syslog(LOG_ERR, "WARNING - Config error [tsmppt60]: %s ", e.msg.c_str());
                    }
                }
            }
        } else {
            syslog(LOG_ERR, "WARNING - Config error: no probe section \n");
        }


    } catch (YAML::Exception &e) {
        syslog(LOG_ERR, "WARNING - Config error: %s \n", e.msg.c_str());
    }


}


const char *Config::getAxpertPortFilename() {
    return axpert_port_filename.c_str();
}

const char *Config::getMqttHostname() {
    return mqtt_hostname.c_str();
}

int Config::getMqttPort() {
    return mqtt_port;
}

const char *Config::getMqttClientId() {
    return mqtt_client_id.c_str();
}

const char *Config::getMqttUsername() {
    return mqtt_username.c_str();
}

const char *Config::getMqttPassword() {
    return mqtt_password.c_str();
}


bool Config::getAxpertEnabled() {
    return axpert_enabled;
}

unsigned int Config::getAxpertQpigsPollInterval() {
    return axpert_qpigs_poll_interval;
}

const char *Config::getVictronBmvPortFilename() {
    return victron_bmv_port_filename.c_str();
}

unsigned int Config::getVictronBmvPollInterval() {
    return victron_bmv_poll_interval;
}

bool Config::getVictronBmvEnabled() {
    return victron_bmv_enabled;
}

std::string Config::getMqttAxpertInverterNameTopic() {
    return axpert_mqtt_inverter_name_topic;
}

std::string Config::getMqttAxpertInverterLoadVoltsTopic() {
    return axpert_mqtt_inverter_load_volts_topic;
}

std::string Config::getMqttAxpertInverterLoadFreqTopic() {
    return axpert_mqtt_inverter_load_freq_topic;
}

std::string Config::getMqttAxpertInverterLoadPercentTopic() {
    return axpert_mqtt_inverter_load_percent_topic;
}

std::string Config::getMqttAxpertInverterLoadWattTopic() {
    return axpert_mqtt_inverter_load_watts_topic;
}

std::string Config::getMqttAxpertInverterBattVoltsTopic() {
    return axpert_mqtt_inverter_batt_volts_topic;
}

std::string Config::getMqttAxpertInverterBattAmpsTopic() {
    return axpert_mqtt_inverter_batt_amps_topic;
}

std::string Config::getMqttAxpertInverterBattWattsTopic() {
    return axpert_mqtt_inverter_batt_watts_topic;
}

std::string Config::getMqttAxpertInverterBattSocTopic() {
    return axpert_mqtt_inverter_batt_soc_topic;
}

std::string Config::getMqttAxpertInverterTempTopic() {
    return axpert_mqtt_inverter_temp_celsius_topic;
}

std::string Config::getMqttAxpertInverterModeTopic() {
    return axpert_mqtt_inverter_mode_topic;
}

std::string Config::getMqttAxpertInverterGridVoltsTopic() {
    return axpert_mqtt_inverter_grid_volts_topic;
}

std::string Config::getMqttAxpertInverterGridFreqTopic() {

    return axpert_mqtt_inverter_grid_freq_topic;
}

std::string Config::getMqttAxpertInverterMpptVolts() {
    return axpert_mqtt_inverter_mppt_volts_topic;
}

std::string Config::getMqttAxpertInverterMpptWatts() {
    return axpert_mqtt_inverter_mppt_watts_topic;
}

bool Config::getTsMppt60Enabled() {
    return tsmppt60_enabled;
}

bool Config::getMqttEnabled() {
    return mqtt_enabled;
}

std::string Config::getTsMppt60Hostname() {
    return tsmppt60_hostname;
}

int Config::getTsMppt60PollInterval() {
    return tsmppt60_poll_interval;
}

int Config::getTsMppt60Port() {
    return tsmppt60_port;
}

std::string Config::getMqttAxpertInverterMpptCurrent() {
    return axpert_mqtt_inverter_mppt_current_topic;
}

std::string Config::getMqttAxpertInverterGridWattsTopic() {
    return axpert_mqtt_inverter_grid_watts_topic;
}
