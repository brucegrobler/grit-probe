//
// Created by bgrobler on 7/12/19.
//

#ifndef AXPERT_CONFIG_UTIL_H
#define AXPERT_CONFIG_UTIL_H

#include <string>

class Config {

private:

    /**
     * MQTT
     */
    bool mqtt_enabled = false;
    std::string mqtt_hostname = "127.0.0.1";
    int mqtt_port = 1883;
    std::string mqtt_username = "user";
    std::string mqtt_password = "password";
    std::string mqtt_client_id = "grit-probe-" + (rand() % 6) + 1;

    /**
     * Voltronic Axpert
     */
    bool axpert_enabled = false;
    std::string axpert_port_filename = "dev/hidraw0";
    unsigned int axpert_qpigs_poll_interval = 5;
    std::string axpert_mqtt_inverter_name_topic = "Inverter/Name";
    std::string axpert_mqtt_inverter_load_volts_topic = "Inverter/InverterVolts";
    std::string axpert_mqtt_inverter_load_freq_topic = "Inverter/InverterFreq";
    std::string axpert_mqtt_inverter_load_percent_topic = "Inverter/LoadPercentage";
    std::string axpert_mqtt_inverter_load_watts_topic = "Inverter/LoadWatts";
    std::string axpert_mqtt_inverter_batt_volts_topic = "Inverter/BatteryVolts";
    std::string axpert_mqtt_inverter_batt_amps_topic = "Inverter/BatteryAmps";
    std::string axpert_mqtt_inverter_batt_watts_topic = "Inverter/BatteryWatts";
    std::string axpert_mqtt_inverter_batt_soc_topic = "Inverter/BatterySOC";
    std::string axpert_mqtt_inverter_temp_celsius_topic = "Inverter/Temperature";
    std::string axpert_mqtt_inverter_mode_topic = "Inverter/InverterMode";
    std::string axpert_mqtt_inverter_grid_volts_topic = "Inverter/GridVolts";
    std::string axpert_mqtt_inverter_grid_watts_topic = "Inverter/GridWatts";
    std::string axpert_mqtt_inverter_grid_freq_topic = "Inverter/GridFreq";
    std::string axpert_mqtt_inverter_mppt_volts_topic = "Inverter/MpptVolts";
    std::string axpert_mqtt_inverter_mppt_watts_topic = "InverterMpptWatts";
    std::string axpert_mqtt_inverter_mppt_current_topic = "Inverter/MpptCurrent";

    /**
     * Victron BMV
     */
    bool victron_bmv_enabled = false;
    std::string victron_bmv_port_filename = "/dev/bmv";
    int victron_bmv_poll_interval = 5;


    /**
     * Morningstar TS-MPPT-60
     */
    bool tsmppt60_enabled = false;
    std::string tsmppt60_hostname = "localhost";
    int tsmppt60_port = 502;
    int tsmppt60_poll_interval = 5;

public:

    const char *getMqttHostname();

    int getMqttPort();

    const char *getMqttUsername();

    const char *getMqttPassword();

    const char *getMqttClientId();

    const char *getAxpertPortFilename();

    unsigned int getAxpertQpigsPollInterval();

    const char *getVictronBmvPortFilename();

    unsigned int getVictronBmvPollInterval();

    bool getVictronBmvEnabled();

    bool getAxpertEnabled();

    bool getTsMppt60Enabled();

    std::string getMqttAxpertInverterNameTopic();


    std::string getMqttAxpertInverterLoadVoltsTopic();

    std::string getMqttAxpertInverterLoadFreqTopic();

    std::string getMqttAxpertInverterLoadPercentTopic();

    std::string getMqttAxpertInverterLoadWattTopic();

    std::string getMqttAxpertInverterBattVoltsTopic();

    std::string getMqttAxpertInverterBattAmpsTopic();

    std::string getMqttAxpertInverterBattWattsTopic();

    std::string getMqttAxpertInverterBattSocTopic();

    std::string getMqttAxpertInverterTempTopic();

    std::string getMqttAxpertInverterModeTopic();

    std::string getMqttAxpertInverterGridVoltsTopic();

    std::string getMqttAxpertInverterGridFreqTopic();

    std::string getMqttAxpertInverterMpptVolts();

    std::string getMqttAxpertInverterMpptWatts();


    bool getMqttEnabled();

    std::string getTsMppt60Hostname();

    int getTsMppt60PollInterval();

    int getTsMppt60Port();


    Config(const char *config_file);

    std::string getMqttAxpertInverterMpptCurrent();

    std::string getMqttAxpertInverterGridWattsTopic();
};


#endif //AXPERT_CONFIG_UTIL_H
