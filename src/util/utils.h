//
// Created by bgrobler on 7/12/19.
//

#ifndef AXPERT_UTILS_H
#define AXPERT_UTILS_H

#include <string>


class Utils {

public:
    static std::string current_date_time();

};


#endif //AXPERT_UTILS_H
